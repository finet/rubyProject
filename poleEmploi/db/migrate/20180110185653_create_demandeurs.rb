class CreateDemandeurs < ActiveRecord::Migration[5.1]
  def change
    create_table :demandeurs do |t|
      t.string :name
      t.string :surname
      t.integer :age
      t.string :city
      t.integer :phone
      t.string :email
      t.string :skill
      t.string :qualification
      t.string :licence

      t.timestamps
    end
  end
end
