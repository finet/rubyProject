class CreateReponses < ActiveRecord::Migration[5.1]
  def change
    create_table :reponses do |t|
      t.integer :id_society
      t.integer :id_demandeur
      t.text :message

      t.timestamps
    end
  end
end
