class CreateAnnonces < ActiveRecord::Migration[5.1]
  def change
    create_table :annonces do |t|
      t.string :title
      t.string :description
      t.integer :salary
      t.string :city
      t.integer :phone
      t.string :email
      t.string :skill
      t.string :qualification
      t.string :licence
    end
  end
end
