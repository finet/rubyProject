Rails.application.routes.draw do
  resources :employers
  resources :reponses
  resources :demandeurs
  resources :annonces
  root 'home#index'
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
