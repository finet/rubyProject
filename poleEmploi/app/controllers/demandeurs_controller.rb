class DemandeursController < ApplicationController
  before_action :set_demandeur, only: [:show, :edit, :update, :destroy]

  # GET /demandeurs
  # GET /demandeurs.json
  def index
    @demandeurs = Demandeur.all
  end

  # GET /demandeurs/1
  # GET /demandeurs/1.json
  def show
  end

  # GET /demandeurs/new
  def new
    @demandeur = Demandeur.new
  end

  # GET /demandeurs/1/edit
  def edit
  end

  # POST /demandeurs
  # POST /demandeurs.json
  def create
    @demandeur = Demandeur.new(demandeur_params)


      if @demandeur.save
        redirect_to @demandeur
      else
        render :new 
      end
    
  end

  # PATCH/PUT /demandeurs/1
  # PATCH/PUT /demandeurs/1.json
  def update

      if @demandeur.update(demandeur_params)
        redirect_to @demandeur
      else
        render :edit
      end
  end

  # DELETE /demandeurs/1
  # DELETE /demandeurs/1.json
  def destroy
    @demandeur.destroy
    redirect_to(demandeurs_path)
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_demandeur
      @demandeur = Demandeur.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def demandeur_params
      params.require(:demandeur).permit(:name, :surname, :age, :city, :phone, :email, :skill, :qualification, :licence)
    end
end
