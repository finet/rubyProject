class ReponsesController < ApplicationController
  before_action :set_reponse, only: [:show, :edit, :update, :destroy]

  # GET /reponses
  # GET /reponses.json
  def index
    @annonce = Annonce.all
  end

  # GET /reponses/1
  # GET /reponses/1.json
  def show
  end

  # GET /reponses/new
  def new
    @reponse = Reponse.new
  end

  # GET /reponses/1/edit
  def edit
  end

  # POST /reponses
  # POST /reponses.json
  def create
    @reponse = Reponse.new(reponse_params)

      if @reponse.save
        redirect_to @reponse
      else
        render :new 
      end
  end

  # PATCH/PUT /reponses/1
  # PATCH/PUT /reponses/1.json
  def update
      if @reponse.update(reponse_params)
        redirect_to @reponse
      else
        render :edit
      end
  end

  # DELETE /reponses/1
  # DELETE /reponses/1.json
  def destroy
    @reponse.destroy
    redirect_to(reponses_path)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reponse
      @reponse = Reponse.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reponse_params
      params.require(:reponse).permit(:id_society, :id_demandeur, :message)
    end
end
