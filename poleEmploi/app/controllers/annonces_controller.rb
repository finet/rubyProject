class AnnoncesController < ApplicationController
  before_action :set_annonce, only: [:show, :edit, :update, :destroy]

  # GET /annonces
  # GET /annonces.json
  def index
    @annonces = Annonce.all 
  end

  # GET /annonces/1
  # GET /annonces/1.json
  def show
  end

  # GET /annonces/new
  def new
    @annonce = Annonce.new
  end

  # GET /annonces/1/edit
  def edit
  end

  # POST /annonces
  # POST /annonces.json
  def create
    @annonce = Annonce.new(annonce_params)


      if @annonce.save
        redirect_to @annonce
      else
        render :new 
      end
    
  end

  # PATCH/PUT /annonces/1
  # PATCH/PUT /annonces/1.json
  def update

      if @annonce.update(annonce_params)
        redirect_to @annonce
      else
        render :edit
      end
  end

  # DELETE /annonces/1
  # DELETE /annonces/1.json
  def destroy
    @annonce.destroy
    redirect_to(annonces_path)
    end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_annonce
      @annonce = Annonce.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def annonce_params
      params.require(:annonce).permit(:title, :description, :salary, :city, :phone, :email, :skill, :qualification, :licence)
    end
end
