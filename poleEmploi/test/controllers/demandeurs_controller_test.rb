require 'test_helper'

class DemandeursControllerTest < ActionDispatch::IntegrationTest
  setup do
    @demandeur = demandeurs(:one)
  end

  test "should get index" do
    get demandeurs_url
    assert_response :success
  end

  test "should get new" do
    get new_demandeur_url
    assert_response :success
  end

  test "should create demandeur" do
    assert_difference('Demandeur.count') do
      post demandeurs_url, params: { demandeur: { age: @demandeur.age, city: @demandeur.city, email: @demandeur.email, licence: @demandeur.licence, name: @demandeur.name, phone: @demandeur.phone, qualification: @demandeur.qualification, skill: @demandeur.skill, surname: @demandeur.surname } }
    end

    assert_redirected_to demandeur_url(Demandeur.last)
  end

  test "should show demandeur" do
    get demandeur_url(@demandeur)
    assert_response :success
  end

  test "should get edit" do
    get edit_demandeur_url(@demandeur)
    assert_response :success
  end

  test "should update demandeur" do
    patch demandeur_url(@demandeur), params: { demandeur: { age: @demandeur.age, city: @demandeur.city, email: @demandeur.email, licence: @demandeur.licence, name: @demandeur.name, phone: @demandeur.phone, qualification: @demandeur.qualification, skill: @demandeur.skill, surname: @demandeur.surname } }
    assert_redirected_to demandeur_url(@demandeur)
  end

  test "should destroy demandeur" do
    assert_difference('Demandeur.count', -1) do
      delete demandeur_url(@demandeur)
    end

    assert_redirected_to demandeurs_url
  end
end
